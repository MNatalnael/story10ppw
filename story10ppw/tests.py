from django.test import TestCase
from django.test import SimpleTestCase
from django.urls import resolve, reverse
from menu.views import *

# Create your tests here.
class Unit_Test(TestCase):
    def test_page_is_exist(self):
        response = self.client.get(reverse('main'))
        self.assertEqual(response.status_code, 200)

    def test_menupage_is_exist(self):
        url = resolve('/')
        self.assertEqual(url.func, main)
